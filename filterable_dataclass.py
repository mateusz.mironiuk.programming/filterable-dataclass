from __future__ import annotations

import itertools
import operator
from copy import copy
from inspect import isgenerator
from typing import Type, List, Any, Union, Generator, Iterator


operators = {
    "lt": operator.lt,
    "le": operator.le,
    "eq": operator.eq,
    "ne": operator.ne,
    "ge": operator.ge,
    "gt": operator.gt,
    "in": lambda a, b: operator.contains(b, a),
    "contains": operator.contains
}


class ObjectSet:
    def __init__(self, objects):
        self._isgeneratorfunction: bool = isgenerator(objects)
        self.objects: Union[Generator, List] = (
            list(objects) if not self._isgeneratorfunction else objects
        )

    def filter(self, **kwargs) -> ObjectSet:
        objects, self.objects = itertools.tee(self.objects)
        for comparison_expression, value in kwargs.items():
            objects = (
                instance for instance in objects
                if self._check(instance, comparison_expression, value)
            )
        return ObjectSet(objects)

    def _check(self, instance: FilterableDataclass, comparison_expression: str, filter_value: Any) -> bool:
        attribute_name, *operator_expression = comparison_expression.split("__")
        instance_value = getattr(instance, attribute_name)
        operator_func = (
            operators.get(operator_expression[0] if operator_expression else "eq")
        )
        return operator_func(instance_value, filter_value)

    def all(self) -> ObjectSet:
        objects, self.objects = itertools.tee(self.objects)
        return ObjectSet(list(objects))

    def exclude(self, **kwargs) -> ObjectSet:
        objects, self.objects = itertools.tee(self.objects)
        for comparison_expression, value in kwargs.items():
            objects = [
                instance for instance in objects
                if not self._check(instance, comparison_expression, value)
            ]
        return ObjectSet(objects)

    def first(self):
        objects, self.objects = itertools.tee(self.objects)
        try:
            return next(objects)
        except StopIteration:
            return None

    def __iter__(self) -> Iterator:
        objects, self.objects = itertools.tee(self.objects)
        return objects

    def __repr__(self):
        if self._isgeneratorfunction:
            objects_repr = "..."
        else:
            objects_repr = ", ".join(repr(obj) for obj in copy(self.objects))
        return f"<{type(self).__name__} [{objects_repr}]>"


class ObjectManager:
    def __init__(self, model: Type[FilterableDataclass]):
        self.model = model

    def filter(self, **django_like_params):
        return ObjectSet(
            objects=(obj for obj in self.model._objects)
        ).filter(**django_like_params)

    def all(self):
        return ObjectSet(
            objects=(obj for obj in self.model._objects)
        ).all()

    def first(self):
        return ObjectSet(
            objects=(obj for obj in self.model._objects)
        ).first()


class FilterableDataclass:
    _objects: List[FilterableDataclass]
    objects: ObjectManager

    def __init_subclass__(cls, **kwargs):
        cls._objects = []
        cls.objects = ObjectManager(cls)
        return super().__init_subclass__()

    def __new__(cls, *args, **kwargs):
        if cls is FilterableDataclass:
            raise RuntimeError(
                "FilterableDataclass in order to be instantiated must be subclassed"
            )
        instance = super().__new__(cls)
        cls._objects.append(instance)
        return instance
