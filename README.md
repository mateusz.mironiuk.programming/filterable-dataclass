### Welcome to FilterableDataclass package! ####
Out-of-the-box, django-like filtering over your class instances!  
#

#### quickstart:

> Lets create dataclass User that inherits from FilterableDataclass
```python
from dataclasses import dataclass, field
from datetime import datetime
from filterable_dataclass import FilterableDataclass

@dataclass
class User(FilterableDataclass):
    id: int
    name: str
    email: str = field(repr=False)
    created_ts: datetime = field(default_factory=datetime.utcnow, init=False, repr=False)
    is_admin: bool = field(default=False, repr=False)
```

> create some instances of User class:

```pyton
norm = User(id=1, name='Norm', email="norm@gmail.com", is_admin=True)
adam = User(id=2, name='Adam', email="adam@gmail.com", is_admin=False)
dave = User(id=3, name='Dave', email="dave@yahoo.com", is_admin=False)
matt = User(id=4, name='Matt', email="norm@outlook.com", is_admin=False)
josh = User(id=5, name='Josh', email="josh@outlook.com", is_admin=False)
```

Given that, you can start using many convenient features:  

> object attribute of a 'User' class contains manager of all its instances 

```python
>>> User.objects
<filterable_dataclass.ObjectManager object at 0x7f0115a283a0>
```

> filter non-admin users:  

```python
>>> non_admin_users = User.objects.filter(is_admin=False)
>>> non_admin_users
<ObjectSet [...]>
```

> filers aren't executed until .all is called
```python
>>> non_admin_users.all()
<ObjectSet [User(id=2, name='Adam'), User(id=3, name='Dave'), User(id=4, name='Matt'), User(id=5, name='Josh')]>
```

> continue filtering: id less than 5
```python
>>> non_admin_users.filter(id__lt=5).all()
<ObjectSet [User(id=2, name='Adam'), User(id=3, name='Dave'), User(id=4, name='Matt')]>
```

> other filter: non-admins with email domain other than 'outlook.com'
```python
>>> User.objects.filter(is_admin=False).exclude(email__contains="outlook.com").all()
<ObjectSet [User(id=2, name='Adam'), User(id=3, name='Dave')]>
```

> get first objects
```python
>>> User.objects.first()
User(id=1, name='Norm')
```